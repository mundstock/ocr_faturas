# -*- coding: utf-8 -*-




from flask import Flask, request, redirect, url_for, render_template, send_from_directory
from werkzeug.utils import secure_filename
import os
import numpy as np
import cv2
import re
from PIL import Image
import pytesseract
from pdf2image import convert_from_path


def pdfimage(filename):
    pages = convert_from_path(filename, 150,single_file=True)
    return pages

#pages = pdfimage('Fatura.pdf')

app = Flask(__name__)

#inFile = cv2.imread("out2.jpg")
def extract(inFile,num):
#    inFile = sys.argv[1]
    
    
#    img = cv2.imread(inFile,0)
    
    	
    img = Image.fromarray(inFile).convert('L')
    
    img2 = img.crop((90, 222, 756, 417))
    
    img3 = img.crop((760, 234, 1188, 408))
    
    img4 = img.crop((79, 409, 454, 822))
    
    img5 = img.crop((459, 498, 1137, 766))
    
    img6 = img.crop((90, 1014, 451, 1330))
    
    img7 = img.crop((852,238,1140,346))
    
    img8 = img.crop((465,525,1140,559))
    
    img9 = img.crop((102,1066,195,1326))
    
    img10 = img.crop((193,1075,357,1320))
    
    img11 = img.crop((376,1071,448,1327))
    
    resultlist = [img4,img5,img3,img2,img6,img7,img8,img9,img10,img11]
    
    result_list_last = list([pytesseract.image_to_string(j)] for j in resultlist)
    
    try:
        s1 = list((result_list_last[3][0]).split('Fase'))[0]
        s2 = "Número da UC: "+(str([int(s) for s in result_list_last[5][0].split() if s.isdigit()])).strip('[]')
        s3 = "Fase : "+str(list(str(list((result_list_last[3][0]).split('Cpf' or 'cpf'))[0]).split('Fase' or 'fase'))[1])
#        s4 = "Cpf: "+''.join(e for e in (str([int(s) for s in (str(list(result_list_last[3][0].split('Cpf' or 'Cpr'))[1])) if s.isdigit()])).strip('[]') if e.isdigit())
        s4 = "Cpf :"+((str(list(re.findall(r'\d{10,14}',str((result_list_last[3][0]).encode('utf-8'))))[0])).replace("\\", ""))

        s5 = "Bandeira Vigente: " +str(list((result_list_last[2][0]).split('Bandeira Vigente' or 'bandeira vigente' or 'bandeira Vigente'))[1])
        s6 = "Classe‎ de‎ Consumo‎ Aneel : "+list(re.split(r'[\n\n]+', list(str(list((result_list_last[3][0]).split('Classe de Consumo Aneel:' or 'classe de Consumo Aneel:'))[1]).split('(/n/n)+'))[0]))[0]
        ss = (str(list(str((list((result_list_last[0][0]).split('Fator' or 'fator'))[0]).encode('utf-8')).split('Medidor' or 'medidor'))[0]))
        s7 = "NP do Medidor : "+(''.join(i for i in ss if i.isdigit()))
        s8 =  "Custo‎ Disp‎ Sistema‎ : "+str((result_list_last[6][0]).encode('utf-8'))
        s9 = ((str(list(re.findall(r'[0-9][0-9][0-9][0-9]',str((result_list_last[7][0]).encode('utf-8'))))[0])).replace("\\", ""))
        s10 = str(list(re.split(r'[0-9][0-9][0-9][0-9]',str((result_list_last[7][0]).encode('utf-8'))))[1])
        s11 = (str(list(re.findall(r'[0-9][0-9][0-9][0-9]',str((result_list_last[7][0]).encode('utf-8'))))[1])).replace("\\", "")
        s12 = str(list(re.split(r'[0-9][0-9][0-9][0-9]',str((result_list_last[7][0]).encode('utf-8'))))[2])
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c = type(e).__name__, m = str(e)))
    
    with open(f'Output{num}.txt','w') as out:
        out.write('\n{}\n{}\n\n{}\n{}\n{}\n\n{}\n{}\n{}\n\n{}\n{}\n{}\n{}\n'.format(s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12))
  
    resultlist = []
    result_list_last = []

    print("Execution completed output.txt file generated")
    
    return 0


   
    
    
ALLOWED_EXTENSIONS = {'pdf'}
def allowed_file(filename):
   return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/uploads/'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



@app.route('/')  
def upload():  
    return render_template("upload.html")  
 
@app.route('/success', methods = ['POST'])  
def success():  
    if request.method == 'POST':  
        f = request.files['file']  
        f.save(os.path.join(UPLOAD_FOLDER,f.filename)) 
        pages = pdfimage((os.path.join(UPLOAD_FOLDER,f.filename)))
        for num,pg in enumerate(pages):
            extract(np.array(pg),num)
        return render_template("success.html", name = f.filename)  
  
if __name__ == '__main__':  
    app.run(debug = True)  
    
    
